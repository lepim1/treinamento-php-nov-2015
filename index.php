<?php
  header("content-type:text/html; charset=utf-8");
  require_once("AutoLoad.php");
  AutoLoad::registerAutoLoader();

  date_default_timezone_set(Config::DEFAULT_TIME_ZONE);

  AntiInjectionHelper::AntiInjection($_GET);
  AntiInjectionHelper::AntiInjection($_POST);

  //rota
  if (isset($_SERVER['REDIRECT_QUERY_STRING'])) {
    parse_str($_SERVER['REDIRECT_QUERY_STRING'], $ParsedUrl);
    if (isset($ParsedUrl["controller"])) {
      $controller = $ParsedUrl["controller"]."Controller";
      // SE EXISTE A QUERY STRING VEJA SE O CONTROLLER TB EXISTE
      if (!class_exists($controller)) {
        $Controller = new Controller();
        $Controller->render("error");
      } else {
        // SE EXISTE O CONTROLLER VEJA SE A ACTION EXISTE
        $Controller = new $controller();
        if (isset($ParsedUrl["action"]) && !empty($ParsedUrl["action"])) {
          $methodVariable = array($Controller, $ParsedUrl["action"]);
          if (method_exists($Controller, $ParsedUrl["action"])) {
            $Controller->$ParsedUrl["action"]($ParsedUrl["query"]);
          } else {
            $Controller->render("error");
          }
        } else {
          $Controller->render("error");
        }
      }
    } else {
      $Controller->render("error");
    }
  } else {
    $Controller = new HomeController;
    $Controller->Index();
  }
