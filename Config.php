<?php
  class Config  {
    // 1-MySQL  2-JSON
    public static $modoPersistencia = 1;
    // Linguagem no qual as mensagens de erro/sucesso devem aparecer
    public static $language = 'pt';
    // O ambiente de teste deve ser setado para true antes de executar os testes
    public static $testEnvironment = false;

    // Constantes de Banco de dados
    const DB_HOST = 'localhost';
    const DB_PORT = '3306';
    const DB_NAME = 'banco';
    const DB_USER = 'root';
    const DB_PASS = '';

    //Timezone padrão
    const DEFAULT_TIME_ZONE = 'America/Sao_Paulo';
  }
