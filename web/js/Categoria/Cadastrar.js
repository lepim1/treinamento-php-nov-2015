$(document).ready(function() {

  $("#frmCadastrar").validate({
    rules: {
      NomeCategoria: {
        required: true
      }
    },
    messages: {
				NomeCategoria: {
          required: "Por favor, entre o nome da categoria"
        }
		},
    submitHandler: function(form) {
      $.ajax({
          type     : $(form).attr('method'),
          url      : $(form).attr('action'),
          data     : $(form).serialize()
      }).done(function(data) {
        console.log(data);
        var obj = JSON.parse(data);
        if (obj.success) {
           var r = confirm(obj.message);
           if (r == true) {
             window.location.href = "/";
           }
        } else {
          alert(obj.message);
        }
      });
    }
   });
});
