function alterar(url, id)
{
  if (validarEmptyField(id)) {
    var nomeCategoria = $("#"+id).val();
    $.ajax({
        type     : "POST",
        url      : url,
        data     : "NomeCategoria="+nomeCategoria+"&id="+id
    }).done(function(data) {
      var obj = JSON.parse(data);
      alert(obj.message);
    });
  }
}

$(document.body).bind('keypress', function(e){
   if ( e.keyCode == 13 ) {
     var id = $(':focus').attr("id");
     var name = $(':focus').attr("name");
     if (name == "NomeCategoria" && id != undefined) {
       alterar('Update',id);
     }
   }
 });
