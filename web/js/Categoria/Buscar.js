$(document).ready(function() {

  $('#frmBuscar').on('submit',function(e) {
    e.preventDefault();
    $.ajax({
        type     : $(this).attr('method'),
        url      : $(this).attr('action'),
        data     : $(this).serialize()
    }).done(function(data) {
      $(".table.table-striped").html(data);
    });
  });

});