$(document).ready(function() {
  $("#Valor").maskMoney({symbol:"R$", decimal:",", thousands:"."});
  $(":input").inputmask();
});

function validarEmptyField(id) {
  var str = $("#"+id).val();
  if (!str || /^\s*$/.test(str)) {
    $("#"+id).attr("placeholder", "Preencha este campo");
    $("#"+id).val("");
    $("#"+id).focus();
    return false;
  }
  return true;
}

function strEmpty(str)
{
  return /^\s+$/.test(str);
}
