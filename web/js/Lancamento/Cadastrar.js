// $(document).ready(function() {
//
//   $('#frmCadastrarLance').on('submit',function(e) {
//     if (validarEmptyField("Descricao")) {
//       if (validarEmptyField("Data")) {
//         if (validarEmptyField("Valor")) {
//           e.preventDefault();
//           var frm = $(this);
//           console.log(frm);
//           $.ajax({
//               type     : $(frm).attr('method'),
//               url      : $(frm).attr('action'),
//               data     : $(frm).serialize()
//           }).done(function(data) {
//             console.log(data);
//             var obj = JSON.parse(data);
//             if (obj.success) {
//                var r = confirm(obj.message);
//                if (r == true) {
//                  window.location.href = "/";
//                }
//             } else {
//               alert(obj.message);
//             }
//           });
//         }
//       }
//     }
//   });
//
// });


$(document).ready(function() {

  $("#frmCadastrarLance").validate({
    rules: {
      Descricao: {
        required: true
      },
      Data: {
        required: true
      },
      Valor: {
       required: true
     },
    },
    messages: {
				Descricao: {
          required: "Por favor, entre com a descrição."
        },
        Data: {
          required: "Por favor, entre uma data."
        },
        Valor: {
          required: "Por favor, entre um valor."
        }
		},
    submitHandler: function(form) {
      $.ajax({
          type     : $(form).attr('method'),
          url      : $(form).attr('action'),
          data     : $(form).serialize()
      }).done(function(data) {
        console.log(data);
        var obj = JSON.parse(data);
        if (obj.success) {
           var r = confirm(obj.message);
           if (r == true) {
             window.location.href = "/";
           }
        } else {
          alert(obj.message);
        }
      });
    }
   });

});
