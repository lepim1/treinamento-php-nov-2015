function filtrarCategoria()
{
  getFiltro("/Lancamento/FiltrarCategoria/"+$('#categoria').val());
}

function filtrarTipo()
{
  getFiltro("/Lancamento/FiltrarTipo/"+$('#tipo').val());
}

function getFiltro(url, valor)
{
  $.ajax({
      type: "get",
      url: url,
  }).done(function(result) {
        $("tbody").html(result);
  }).fail(function() {
      alert("Erro");
  });
}

$( "#categoria" ).change(function() {
  filtrarCategoria();
});

$( "#tipo" ).change(function() {
  filtrarTipo();
});