<?php

class LancamentoController extends Controller {
  const PREENCHIMENTO = 5;
  const DADOS_REPETIDOS = 6;
  const CREATE_SUCCESS = 9;
  const CREATE_FAIL = 10;

  public function index() {

  }

  public function create() {
    if (ValidateHelper::isNullOrEmptyString($_POST["Descricao"]) || ValidateHelper::isNullOrEmptyString($_POST["Data"]) ||
        !isset($_POST["Categoria"]) || !isset($_POST["Tipo"]) ||
        ValidateHelper::isNullOrZeroFloat($_POST["Valor"])) {
          return parent::echoJSON(self::FAIL, self::PREENCHIMENTO);
    }
    $lancamentoDAO                            = FabricaDAO::getInstanceDAO("Lancamento");
    $lancamentoDAO->Lancamento["descricao"]   = $_POST["Descricao"];

    $date                                     = DateTime::createFromFormat('d/m/Y', $_POST["Data"]);
    $lancamentoDAO->Lancamento["data"]        = $date->format('Y-m-d');
    if ($lancamentoDAO->isDescricaoDataRepetida()) {
      return parent::echoJSON(0, self::DADOS_REPETIDOS);
    }
    $lancamentoDAO->Lancamento["valor"]        = NumberHelper::converteMoedaFloat($_POST["Valor"]);
    $lancamentoDAO->Lancamento["categoriaId"]  = $_POST["Categoria"];
    $lancamentoDAO->Lancamento["tipo"]         = $_POST["Tipo"];

    $retorno = $lancamentoDAO->create();

    if (is_a($retorno, "BancoException")) {
      return parent::echoJSON(self::FAIL, self::CREATE_FAIL);
    }
    parent::echoJSON(self::SUCCESS, self::CREATE_SUCCESS);
  }

  public function retrieve() {

  }

  public function update() {

  }

  public function delete() {

  }

  public function cadastrar() {
    $model = new stdClass();
    $model->Categorias = $this->getCategorias();
    parent::render("Lancamento/Cadastrar", $model);
  }

  public function buscar() {
    $model = new stdClass();
    $model->Categorias = $this->getCategorias();
    $model->Lancamentos = $this->getLancamentos();

    parent::render("Lancamento/Buscar", $model);
  }

  public function filtrarCategoria() {
      $lancamentoDAO = FabricaDAO::getInstanceDAO("Lancamento");
      $lancamentoDAO->Lancamento["categoriaId"]  = $_GET["query"];
      $model = new stdClass();
      if (ValidateHelper::isNullOrEmptyString($_GET["query"])) {
          $model->Lancamentos = $lancamentoDAO->retrieveAll();
      } else {
        $model->Lancamentos = $lancamentoDAO->retrieveByCategoria();
      }
      parent::render("Lancamento/Listar", $model);
  }

  public function filtrarTipo() {
      $lancamentoDAO = FabricaDAO::getInstanceDAO("Lancamento");
      $lancamentoDAO->Lancamento["tipo"]  = $_GET["query"];
      $model = new stdClass();
      if (ValidateHelper::isNullOrEmptyString($_GET["query"])) {
          $model->Lancamentos = $lancamentoDAO->retrieveAll();
      } else {
        $model->Lancamentos = $lancamentoDAO->retrieveByTipo();
      }
      parent::render("Lancamento/Listar", $model);
  }

  public function getCategorias() {
    $categoriaController = new CategoriaController;
    return $categoriaController->getCategorias()->Categorias;
  }

  public function getLancamentos() {
    $lancamentoDAO = FabricaDAO::getInstanceDAO("Lancamento");
    return $lancamentoDAO->RetrieveAll();
  }


}
