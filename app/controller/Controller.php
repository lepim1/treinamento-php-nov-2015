<?php

class Controller {
  // Constantes de retorno
  const FAIL = 0;
  const SUCCESS = 1;

  // inclue um arquivo da view para ser renderizado
  public static function render($page, $model=null) {
    require "app/view/".$page.".phtml";
  }

  // Retorna um objeto JSON para ser tratado no JavaScript
  public function getJSONReturn($success, $message=0, $data=array()) {
    return json_encode(array("success" => $success, "message" => ReturnMessageHelper::getMessage($message), "data" => $data));
  }

  // Imprime um objeto JSON
  public function echoJSON($success, $message=0, $data=array()) {
    echo $this->getJSONReturn($success, $message, $data);
  }

}
