<?php

class CategoriaController extends Controller {
  const NO_PARAMETERS = 3;
  const RETRIEVE_FAIL = 4;
  const CREATE_SUCCESS = 7;
  const CREATE_FAIL = 8;
  const UNDEFINED_CATEGORIA = 9;
  const REMOVE_SUCCESS = 11;
  const REMOVE_FAIL = 12;
  const UPDATE_SUCCESS = 13;
  const UPDATE_FAIL = 14;

  public function create() {
    if (ValidateHelper::isNullOrEmptyString($_POST["NomeCategoria"])) {
      return parent::echoJSON(self::FAIL, self::NO_PARAMETERS);
    }
    $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
    $categoriaDAO->Categoria["nome"] = $_POST["NomeCategoria"];
    $retorno = $categoriaDAO->create();
    if (is_a($retorno, "BancoException")) {
      return parent::echoJSON(self::FAIL, self::CREATE_FAIL);
    }
    return parent::echoJSON(self::SUCCESS, self::CREATE_SUCCESS);
  }

  public function retrieve() {
    if (!isset($_POST["NomeCategoria"])) {
      return parent::echoJSON(self::FAIL);
    }
    $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
    $categoriaDAO->Categoria["nome"] = $_POST["NomeCategoria"];
    $retorno = $categoriaDAO->retrieve();

    if (is_a($retorno, "BancoException")) {
      return parent::echoJSON(self::FAIL, self::RETRIEVE_ERROR);
    }

    $model = new stdClass();
    $model->Categorias = $retorno;
    parent::render("Categoria/Listar", $model);
  }

  public function update() {
    if (ValidateHelper::isNullOrEmptyString($_POST["NomeCategoria"]) || ValidateHelper::isNullOrEmptyString($_POST["id"])) {
      return parent::echoJSON(self::FAIL);
    }
    $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
    $categoriaDAO->Categoria["nome"] = $_POST["NomeCategoria"];
    $categoriaDAO->Categoria["id"] = $_POST["id"];
    $retorno = $categoriaDAO->update();
    if (is_a($retorno, "BancoException")) {
      return parent::echoJSON(self::FAIL, self::UPDATE_FAIL);
    }
    return parent::echoJSON(self::SUCCESS, self::UPDATE_SUCCESS);
  }

  public function delete() {
    if (ValidateHelper::isNullOrEmptyString($_GET["query"])) {
      return parent::echoJSON(self::REMOVE_FAIL);
    }
    $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
    $categoriaDAO->Categoria["id"] = $_GET["query"];
    $retorno = $categoriaDAO->delete();
    if (is_a($retorno, "BancoException")) {
      return parent::echoJSON(self::FAIL, self::REMOVE_FAIL);
    }
    return parent::echoJSON(self::SUCCESS, self::REMOVE_SUCCESS);      
  }

  public function cadastrar() {
    parent::render("Categoria/Cadastrar");
  }

  public function buscar() {
    parent::render("Categoria/Buscar");
  }

  public function alterar() {
    parent::render("Categoria/Alterar", $this->getCategorias());
  }

  public function remover() {
    parent::render("Categoria/Remover", $this->getCategorias());
  }

  public function getCategorias() {
    $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
    $categorias = $categoriaDAO->RetrieveAll();
    $model = new stdClass();
    $model->Categorias = $categorias;
    return $model;
  }

}
