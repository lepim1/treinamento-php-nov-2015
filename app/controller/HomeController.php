<?php

  class HomeController extends Controller {
    public function index() {
      // Obtem as categorias cadastradas
      $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
      $categorias = $categoriaDAO->RetrieveAll();

      // Obtem os lançamentos cadastrados
      $lancamentoDAO = FabricaDAO::getInstanceDAO("Lancamento");
      $lancamentos = $lancamentoDAO->RetrieveAll();

      // Seta o model para mandar pra view
      $model = new stdClass();
      $model->Categorias = $categorias;
      $model->Lancamentos = $lancamentos;

      // Renderiza a home
      parent::render("home", $model);
    }
  }
