<?php

class MySQLPDO extends PDO {
    /**
     * CONSTRUCT A PDO
     * @param type $options
     * @return null
     */
    public function __construct($options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')) {
        try {
            @parent::__construct('mysql:host='.Config::DB_HOST.';port='.Config::DB_PORT.';dbname='.Config::DB_NAME,
            Config::DB_USER, Config::DB_PASS, $options);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $err) {
          BancoLog::v($err);
          exit(ReturnMessageHelper::getMessage(1));
        }
    }

    /**
     * MAKE SAFE QUERIES WITH PARAMETERS
     * @param type $query
     * @return PDO STATEMENT
     */
    public function query($query) {
      //SAFE QUERY WITH PREPARE STATEMENT
        try {
          //PUTS THE ARGUMENTS PASSED TO THIS METHOD INTO AN ARRAY
          $args = func_get_args();
          //array_shift() TAKES OFF AND RETURNS THE FIRST ELEMENT(QUERY) OF THE ARRAY OF ARGUMENTS
          array_shift($args);
          //PREPARE THE QUERY
          $statm = parent::prepare($query);
          //EXECUTE THE STATEMENT
          $statm->execute($args);
        } catch (PDOException $err) {
          return new BancoException(Config::$language, 2, $err, $statm);
        }
        return $statm;
    }

    /**
     * @param type $query
     * @return array or BancoException
     */
    public function queryArray($query) {
        $retorno = $this->query($query);
        if (is_a($retorno, 'BancoException')) {
          return $retorno;
        }
        return $retorno->fetchAll();
    }
}
