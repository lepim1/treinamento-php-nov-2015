<?php
  // Fábrica de objetos DAO
  class FabricaDAO {
    // Recebe o prefixo da classe
    public static function getInstanceDAO($object) {
      // De acordo com o modo de persistência retorna um objeto DAO
      switch (Config::$modoPersistencia) {
        case 1:
          $persistenciaTipo = "MySQL";
        break;
        case 2:
          $persistenciaTipo = "Json";
        break;
        default:
          $persistenciaTipo = "MySQL";
        break;
      }
      $object = $object."DAO".$persistenciaTipo;
      return new $object();
    }
  }
