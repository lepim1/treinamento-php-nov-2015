<?php

  interface LancamentoDAOInterface {
    public function create();
    public function retrieve();
    public function retrieveAll();
    public function update();
    public function delete();
    public function retrieveByCategoria();
    public function retrieveByTipo();
    public function isDescricaoDataRepetida();
  }
