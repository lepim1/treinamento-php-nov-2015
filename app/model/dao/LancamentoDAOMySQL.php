<?php

class LancamentoDAOMySQL extends MySQLPDO implements LancamentoDAOInterface {
  public function create() {
    //id, descrição, valor, data, categoria, tipo: receita ou despesa
    return $this->query("INSERT INTO lancamento(descricao, valor, data, categoriaId, tipo) VALUES (?,?,?,?,?)"
    ,$this->Lancamento["descricao"]
    ,$this->Lancamento["valor"]
    ,$this->Lancamento["data"]
    ,$this->Lancamento["categoriaId"]
    ,$this->Lancamento["tipo"]
    );
  }

  public function retrieve() {
    return $this->query("SELECT * FROM lancamento WHERE id = ?", $this->Lancamento["id"]);
  }

  public function retrieveAll() {
    return $this->query("SELECT * FROM categoria c, lancamento l WHERE l.categoriaId = c.id")->fetchAll(PDO::FETCH_ASSOC);
  }

  public function update() {

  }

  public function delete() {
    return $this->query("DELETE FROM lancamento WHERE id = ?", $this->Lancamento["id"]);
  }

  public function retrieveByCategoria() {
    return $this->query("SELECT l.*, c.nome FROM lancamento l, categoria c WHERE l.categoriaId = ? AND l.categoriaId = c.id", $this->Lancamento["categoriaId"])->fetchAll(PDO::FETCH_ASSOC);
  }

  public function retrieveByTipo() {
    return $this->query("SELECT l.*, c.nome FROM lancamento l, categoria c WHERE l.tipo = ? AND l.categoriaId = c.id", $this->Lancamento["tipo"])->fetchAll(PDO::FETCH_ASSOC);
  }

  public function isDescricaoDataRepetida() {
    $res = $this->query("SELECT COUNT(*) FROM lancamento l WHERE l.data = ? AND l.descricao = ?", $this->Lancamento["data"], $this->Lancamento["descricao"]);
    return $res->fetchColumn() > 0;
  }

}
