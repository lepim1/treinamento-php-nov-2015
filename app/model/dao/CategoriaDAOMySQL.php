<?php

class CategoriaDAOMySQL extends MySQLPDO implements CategoriaDAOInterface {
  public function create() {
    return $this->query("INSERT INTO categoria(nome) VALUES (?)", $this->Categoria["nome"]);
  }

  public function retrieve() {
    return $this->query("SELECT * FROM categoria WHERE nome like ?", "%".$this->Categoria["nome"]."%")->fetchAll(PDO::FETCH_ASSOC);
  }

  public function retrieveAll() {
    return $this->query("SELECT * FROM categoria")->fetchAll(PDO::FETCH_ASSOC);
  }

  public function update() {
    return $this->query("UPDATE categoria SET nome = ? WHERE id = ?", $this->Categoria["nome"], $this->Categoria["id"]);
  }

  public function delete() {
    return $this->query("DELETE FROM categoria WHERE id = ?", $this->Categoria["id"]);
  }
}
