<?php

  class CategoriaDAOJson implements CategoriaDAOInterface {

    public $file = 'app/model/dao/json/categoria/categorias.json';

    public function create() {
      if (!file_exists($this->file)) {
        $this->Categoria["id"] = 1;
        FileWriterHelper::writeData($this->file, "a+", json_encode(array($this->Categoria)));
      } else {
        $array = json_decode(file_get_contents($this->file), true);
        if (is_null($array)) {
          $array = array();
        }
        $max = 0;
        foreach ($array as $value) {
          if ($value["id"] > $max) {
            $max = $value["id"];
          }
        }
        $max = $max + 1;
        $this->Categoria["id"] = $max;
        $array[] = $this->Categoria;
        FileWriterHelper::writeData($this->file, "w", json_encode($array));
      }
    }


    public function retrieve() {
      $categorias = FileWriterHelper::retrieveArray($this->file);
      if (!isset($this->Categoria) || count($this->Categoria) == 0) {
        return;
      }
      $pesquisarPorArray = array_keys($this->Categoria);
      $retorno = array();
      foreach ($pesquisarPorArray as $atributo) {
        foreach ($categorias as $categoria) {
          if (strlen($this->Categoria[$atributo]) == 0 || $categoria[$atributo] == $this->Categoria[$atributo]) {
            $retorno[] = $categoria;
          } else {
            $busca = strtolower($this->Categoria[$atributo]);
            $observado = strtolower($categoria[$atributo]);
            if (substr_compare($observado, $busca, 0, strlen($busca)) === 0) {
              $retorno[] = $categoria;
            } else {
              similar_text($observado, $busca, $percent);
              if ($percent > 49) {
                $retorno[] = $categoria;
              }
            }
          }
        }
      }
      return $retorno;
    }


    public function retrieveAll() {
      return FileWriterHelper::retrieveArray($this->file);
    }


    public function update() {
      $array = $this->retrieveAll();
      foreach ($array as &$value) {
        if ($value["id"] == $this->Categoria["id"]) {
          $value["nome"] = $this->Categoria["nome"];
        }
      }
      FileWriterHelper::writeData($this->file, "w", json_encode($array));
    }


    public function delete() {
      $array = $this->retrieveAll();

      foreach ($array as $key => $value) {
        if ($array[$key]["id"] == $this->Categoria["id"]) {
          unset($array[$key]);
        }
      }

      FileWriterHelper::writeData($this->file, "w", json_encode($array));
    }
  }
