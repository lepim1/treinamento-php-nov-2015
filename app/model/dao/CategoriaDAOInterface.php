<?php

  interface CategoriaDAOInterface {
    public function create();
    public function retrieve();
    public function retrieveAll();
    public function update();
    public function delete();
  }
