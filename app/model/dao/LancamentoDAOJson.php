<?php

  class LancamentoDAOJson implements LancamentoDAOInterface {

    public $file = 'app/model/dao/json/lancamento/lancamentos.json';

    public function create() {
      if (!file_exists($this->file)) {
        $this->Lancamento["id"] = 1;
        FileWriterHelper::writeData($this->file, "a+", json_encode(array($this->Lancamento)));
      } else {
        $array = json_decode(file_get_contents($this->file), true);
        if (is_null($array)) {
          $array = array();
        }
        $max = 0;
        foreach ($array as $value) {
          if ($value["id"] > $max) {
            $max = $value["id"];
          }
        }
        $max = $max + 1;
        $this->Lancamento["id"] = $max;
        $array[] = $this->Lancamento;
        FileWriterHelper::writeData($this->file, "w", json_encode($array));
      }
    }

    public function retrieve() {
      $lancamentos = FileWriterHelper::retrieveArray($this->file);
      if (!isset($this->Lancamento) || count($this->Lancamento) == 0) {
        return;
      }
      $pesquisarPorArray = array_keys($this->Lancamento);
      $retorno = array();
      foreach ($pesquisarPorArray as $atributo) {
        foreach ($lancamentos as $lancamento) {
          if ($lancamento[$atributo] == $this->Lancamento[$atributo]) {
            $retorno[] = $lancamento;
          }
        }
      }
      return $retorno;
    }

    public function retrieveByCategoria() {
      $lancamentos = FileWriterHelper::retrieveArray($this->file);
      $categoriaDAOJson = new CategoriaDAOJson();
      $categorias = $categoriaDAOJson->retrieveAll();

      if (!isset($this->Lancamento) || count($this->Lancamento) == 0) {
        return;
      }

      $lancamentosCategoria = array();
      foreach ($lancamentos as $key => $lancamento) {
        if (isset($lancamento["categoriaId"]) && $lancamento["categoriaId"] == $this->Lancamento["categoriaId"]) {
          foreach ($categorias as $key => $categoria) {
            if ($lancamento["categoriaId"] == $categoria["id"]) {
              $lancamento["nome"] = $categoria["nome"];
              break;
            }
          }
          $lancamentosCategoria[] = $lancamento;
        }
      }
      return $lancamentosCategoria;
    }

    public function retrieveByTipo() {
      $lancamentos = FileWriterHelper::retrieveArray($this->file);
      $categoriaDAOJson = new CategoriaDAOJson();
      $categorias = $categoriaDAOJson->retrieveAll();

      if (!isset($this->Lancamento) || count($this->Lancamento) == 0) {
        return;
      }

      $lancamentosTipo = array();
      foreach ($lancamentos as $lancamento) {
        if (isset($lancamento["tipo"]) && $lancamento["tipo"] == $this->Lancamento["tipo"]) {
          //PEGAR TB O NOME DA CATEGORIA
          if (isset($lancamento["categoriaId"])) {
            foreach ($categorias as $categoria) {
              if ($lancamento["categoriaId"] == $categoria["id"]) {
                $lancamento["nome"] = $categoria["nome"];
                break;
              }
            }
          }
          $lancamentosTipo[] = $lancamento;
        }
      }
      return $lancamentosTipo;
    }

    public function retrieveAll() {
      $lancamentos = FileWriterHelper::retrieveArray($this->file);
      $categoriaDAOJson = new CategoriaDAOJson();
      $categorias = $categoriaDAOJson->retrieveAll();

      if (sizeof($lancamentos) > 0) {
        foreach ($lancamentos as &$lancamento) {
          if (isset($lancamento["categoriaId"])) {
            foreach ($categorias as $categoria) {
              if ($lancamento["categoriaId"] == $categoria["id"]) {
                $lancamento["nome"] = $categoria["nome"];
                break;
              }
            }
          }
        }
        return $lancamentos;
      } else {
        return array();
      }
    }

    public function update() {
      $array = $this->retrieveAll();
      foreach ($array as &$value) {
        if ($value["id"] == $this->Lancamento["id"]) {
          $value["descricao"] = $this->Lancamento["descricao"];
          $value["valor"] = $this->Lancamento["valor"];
          $value["data"] = $this->Lancamento["data"];
          $value["categoriaId"] = $this->Lancamento["categoriaId"];
          $value["tipo"] = $this->Lancamento["tipo"];
        }
      }
      FileWriterHelper::writeData($this->file, "w", json_encode($array));
    }

    public function delete() {
      $array = $this->retrieveAll();
      for ($i=0; $i<count($array); $i++) {
        if ($array[$i]["id"] == $this->Lancamento["id"]) {
          unset($array[$i]);
        }
      }
      FileWriterHelper::writeData($this->file, "w", json_encode($array));
    }

    public function isDescricaoDataRepetida() {
      $array = $this->retrieveAll();
      foreach ($array as &$value) {
        if (strnatcasecmp($value["descricao"], $this->Lancamento["descricao"]) == 0 && strnatcasecmp($value["data"], $this->Lancamento["data"]) == 0) {
          return true;
        }
      }
      return false;
    }

    public function filtrarCategoria() {
      $array = $this->retrieveAll();
      foreach ($array as &$value) {
        if (strnatcasecmp($value["descricao"], $this->Lancamento["descricao"]) == 0 && strnatcasecmp($value["data"], $this->Lancamento["data"]) == 0) {
          return true;
        }
      }
    }
  }
