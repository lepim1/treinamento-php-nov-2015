<?php
require_once 'GenericTestsDatabaseTestCase.php';

class CreateCategoriaTest extends GenericTestsDatabaseTestCase {
  public function getDataSet() {
    return $this->createMySQLXMLDataSet(getcwd().'/tests/banco.xml');
  }

  public static function setUpBeforeClass() {
    chdir('../');
    AutoLoad::registerAutoLoader();
    fwrite(STDOUT, __METHOD__ . "\n");
  }

  protected function setUp() {
    fwrite(STDOUT, __METHOD__ . "\n");
  }

  protected function assertPreConditions() {
    fwrite(STDOUT, __METHOD__ . "\n");
  }

  public function testCreate() {
    fwrite(STDOUT, __DIR__ . "\n");
    fwrite(STDOUT, __METHOD__ . "\n");

    $categoriaController = new CategoriaController();
    $_POST["NomeCategoria"] = "Teste Categoria 2";

    if (Config::$modoPersistencia == 1) {
      $linhasAntes = $this->getConnection()->getRowCount('categoria');
      fwrite(STDOUT, "Linhas antes: ".  $linhasAntes . "\n");

      $resultingTable = $this->getConnection()
              ->createQueryTable("categoria",
              "SELECT * FROM categoria");
      fwrite(STDOUT, "Linhas antes: ".  $resultingTable . "\n");

      $categoriaController->Create();

      $resultingTable = $this->getConnection()
              ->createQueryTable("categoria",
              "SELECT * FROM categoria");
      $linhasDepois = $this->getConnection()->getRowCount('categoria');
      fwrite(STDOUT, "Linhas depois: ".  $resultingTable . "\n");
      fwrite(STDOUT, "Linhas depois: ". $linhasDepois . "\n");
    } else {
      $categoriaDAO = FabricaDAO::getInstanceDAO("Categoria");
      $linhasAntes = count($categoriaDAO->retrieveAll());
      fwrite(STDOUT, "Linhas antes: ".   $linhasAntes . "\n");
      fwrite(STDOUT, "Linhas antes: ".   $categoriaDAO->retrieveAll() . "\n");
      $categoriaController->Create();

      $linhasDepois = count($categoriaDAO->retrieveAll());
      fwrite(STDOUT, "Linhas depois: ".   $linhasDepois . "\n");
      fwrite(STDOUT, "Linhas depois: ".   $categoriaDAO->retrieveAll() . "\n");
    }

    $this->assertEquals($linhasAntes+1, $linhasDepois);
  }

  public function testTwo() {
    fwrite(STDOUT, __METHOD__ . "\n");
    $this->assertTrue(false);
  }

  protected function assertPostConditions() {
    fwrite(STDOUT, __METHOD__ . "\n");
  }

  protected function tearDown() {
    fwrite(STDOUT, __METHOD__ . "\n");
  }

  public static function tearDownAfterClass() {
    fwrite(STDOUT, __METHOD__ . "\n");
  }

  protected function onNotSuccessfulTest(Exception $e) {
    fwrite(STDOUT, __METHOD__ . "\n");
    throw $e;
  }
}
