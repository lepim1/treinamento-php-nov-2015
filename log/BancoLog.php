<?php

class BancoLog {
  public static $file = "log/log.txt";

  public static function v(Exception $err) {    
      $data = "\r\n" . date("Y-m-d H:i:s") . " " . get_class($err) . utf8_decode($err->getMessage()) . " in ". $err->getFile() . " on line " . $err->getLine();
      FileWriterHelper::writeData(BancoLog::$file, "a+", $data);
  }

}
