<?php
class BancoException extends \Exception {
    public $errorCode;
    public $errorMessage;
    public $statm;
    public $language;
    public $exception;

    public function __construct($language, $errorcode, $exception, $statm) {
        $this->language = $language;
        $this->errorCode = $errorcode;
        $this->errorMessage = $this->getMessageMap();
        $this->exception = $exception;
        BancoLog::v($exception);
        $this->statm = $statm;
    }

    public function getMessageMap() {
        $errors = file("errors/".$this->language.".txt");
        foreach ($errors as $error) {
            list($key,$value) = explode(",", $error, 2);
            $errorArray[$key] = $value;
        }
        return $errorArray[$this->errorCode];
    }
}
