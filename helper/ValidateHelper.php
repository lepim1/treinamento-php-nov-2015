<?php

class ValidateHelper {
  public static function isNullOrEmptyString($string) {
      return (!isset($string) || trim($string)==='');
  }

  public static function isNullOrZeroFloat($value) {
      return (!isset($value) || trim($value)===0);
  }
}
