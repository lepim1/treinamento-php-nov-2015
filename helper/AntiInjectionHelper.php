<?php
class AntiInjectionHelper {

  public static function antiInjection(&$variable) {
    array_walk_recursive($variable, function (&$item, &$key) {
      $item = strip_tags($item);
      $key = strip_tags($key);
    });
  }
}
