<?php

class ReturnMessageHelper {
  public static $messageArray;

  public static function getMessage($messageCode) {
    if (is_null(ReturnMessageHelper::$messageArray)) {
      $messages = file("errors/pt.txt");
      foreach ($messages as $message) {
        list($key, $value) = explode(",", $message, 2);
        ReturnMessageHelper::$messageArray[$key] = $value;
      }
    }
    return ReturnMessageHelper::$messageArray[$messageCode];
  }
}
