<?php

class NumberHelper {
  public static function converteMoedaFloat($valor) {
     if ($valor === "") {
       $valor =  0;
     } else {
       $original = array("R$", ".", ",");
       $substituir = array("", "", ".");
       $valor = str_replace($original, $substituir, $valor);
       $valor = (float)trim($valor);
     }
     return $valor;
  }

  public static function converteFloatMoeda($valor) {
    return 'R$ '. number_format($valor, 2, ',', '.');
  }
}
