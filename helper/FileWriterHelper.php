<?php

class FileWriterHelper {
    // suggested mode 'a' for writing to the end of the file
    public static function writeData($path, $mode, $data) {
        $fp = @fopen($path, $mode);

        if (!$fp) {
            die(json_encode(array("success" => 0, "message" =>"Arquivo não pode ser aberto")));
        }

        $retries = 0;
        $maxRetries = 100;

        // keep trying to get a lock as long as possible
        do {
            if ($retries > 0) {
                usleep(rand(1, 10000));
            }
            $retries += 1;
        } while (!flock($fp, LOCK_EX) && $retries <= $maxRetries);

        // couldn't get the lock, give up
        if ($retries == $maxRetries) {
            // failure
            return false;
        }

        // got the lock, write the data
        fwrite($fp, $data);
        // release the lock
        flock($fp, LOCK_UN);
        fclose($fp);
        // success
        return true;
    }


    public static function retrieve($file) {
      if (!file_exists($file)) {
        return;
      }
      $fp = fopen($file, 'r');
      $object = json_decode(fread($fp, filesize($file)));
      fclose($fp);
      return $object;
    }

    public static function retrieveArray($file) {
      if (!file_exists($file)) {
        return array();
      }
      $retorno = json_decode(file_get_contents($file), true);
      if (!is_array($retorno)) {
        return array();
      }
      return $retorno;
    }

    public static function delete($file) {
      if (file_exists($file)) {
        rename($file, $file."_");
      }
    }

    public static function deleteLastFile($dirParam, $directorySeparator) {
      $dir = scandir($dirParam);
      if (count($dir)<=0) {
        return;
      }
      natsort($dir);
      $file = $dirParam.$directorySeparator.end($dir);
      rename($file, $file."_");
    }
}
