<?php

class AutoLoad {
  public static function autoLoader($class) {
    $arrayDir = array("./", "app/controller/", "app/model/", "app/model/dao/",
    "app/view/", "helper/", "errors/", "log/", "tests/");

    foreach ($arrayDir as $dir) {
        $dir = $dir.$class.".php";
        if (file_exists($dir)) {
          require_once $dir;
          break;
        }
    }    
  }

  public static function registerAutoLoader() {
    spl_autoload_register(array(__CLASS__,'autoLoader'));
  }
}
